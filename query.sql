5.1
CREATE DATABASE QLSV;
USE QLSV;

CREATE TABLE DMKHOA (
    MaKH varchar(6) NOT NULL,
    TenKhoa varchar(30),
    PRIMARY KEY (MaKH)
);

CREATE TABLE SINHVIEN (
    MaSV varchar(6) NOT NULL,
    HoSV varchar(30) NOT NULL,
    TenSV varchar(15) NOT NULL,
    GioiTinh char(1),
    NgaySinh DateTime,
    NoiSinh varchar(50),
    DiaChi varchar(50),
    MaKH varchar(6),
    HocBong Int,
    PRIMARY KEY (MaSV),
    FOREIGN KEY (MaKH) REFERENCES DMKHOA(MaKH)
);

5.2
SELECT SINHVIEN.*
FROM SINHVIEN
INNER JOIN DMKHOA ON SINHVIEN.MaKH = DMKHOA.MaKH
WHERE DMKHOA.TenKhoa = 'công nghệ thông tin';
